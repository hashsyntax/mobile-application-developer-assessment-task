package com.syntax.assessmenttask;

import android.support.test.InstrumentationRegistry;

import com.syntax.assessmenttask.utils.NetworkUtils;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MainActivityNetworkTest {

    @Test
    public void networkTest(){
        assertTrue( NetworkUtils.getInstance().isNetworkConnected(InstrumentationRegistry.getInstrumentation().getContext()));
    }
}