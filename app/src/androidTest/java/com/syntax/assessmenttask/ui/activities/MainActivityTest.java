package com.syntax.assessmenttask.ui.activities;

import android.support.test.rule.ActivityTestRule;
import android.view.View;
import android.widget.FrameLayout;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.syntax.assessmenttask.R;
import com.syntax.assessmenttask.ui.fragments.MainFragment;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);
    private MainActivity mainActivity = null;
    @Before
    public void setUp() throws Exception {
        mainActivity = activityTestRule.getActivity();
    }

    @Test
    public void testLaunch(){
        FrameLayout flContainer = mainActivity.findViewById(R.id.container);
        assertNotNull(flContainer);
        MainFragment mainFragment = MainFragment.newInstance();
        mainActivity.getSupportFragmentManager().beginTransaction().add(flContainer.getId(), mainFragment).commitAllowingStateLoss();

        getInstrumentation().waitForIdleSync();

        View view = mainFragment.getView().findViewById(R.id.progressBar1);

        assertNotNull(view);
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }

}