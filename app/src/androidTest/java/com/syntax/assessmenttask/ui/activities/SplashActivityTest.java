package com.syntax.assessmenttask.ui.activities;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;
import com.syntax.assessmenttask.R;

@RunWith(AndroidJUnit4.class)
public class SplashActivityTest {
    @Rule
    public ActivityTestRule<SplashActivity> activityTestRule = new ActivityTestRule<>(SplashActivity.class);
    private int splashScreenWaitingTime = 3000;
    private SplashActivity splashActivity = null;
    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(MainActivity.class.getName(), null, false);
    @Before
    public void setUp() throws Exception {
        splashActivity = activityTestRule.getActivity();
    }

    @Test
    public void testSecondActivityLaunch() throws InterruptedException {
        assertNotNull(splashActivity.findViewById(R.id.progressBar));
        Activity activity = getInstrumentation().waitForMonitorWithTimeout(monitor, splashScreenWaitingTime);
        assertNotNull(activity);

        activity.finish();
    }
    @After
    public void tearDown() throws Exception {
        splashActivity = null;
    }
}