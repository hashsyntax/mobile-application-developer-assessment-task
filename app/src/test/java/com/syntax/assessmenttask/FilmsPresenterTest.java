package com.syntax.assessmenttask;

import com.syntax.assessmenttask.models.FilmsModelResponse;
import com.syntax.assessmenttask.presenters.FilmsPresenter;
import com.syntax.assessmenttask.views.ViewContractor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class FilmsPresenterTest {
    @Mock
    private ViewContractor.ViewFilmsList viewFilmsList;
    private FilmsModelResponse filmsModelResponse;
    private FilmsPresenter filmsPresenter;

    @Captor
    private ArgumentCaptor<FilmsModelResponse> modelResponseArgumentCaptor;
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        filmsPresenter = new FilmsPresenter(viewFilmsList);
    }
    @Test
    public void handleSuccess() throws Exception{
        filmsPresenter.requestFilms();
    }
    @After
    public void tearDown() throws Exception {

    }
}
