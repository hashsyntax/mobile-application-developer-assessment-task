package com.syntax.assessmenttask.presenters;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.syntax.assessmenttask.interfaces.VolleyResponseListener;
import com.syntax.assessmenttask.utils.ConstantsUtils;
import com.syntax.assessmenttask.utils.VolleyUtils;
import com.syntax.assessmenttask.views.ViewContractor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;

public class FilmsPresenter implements ViewContractor.FilmsListPresenter {
    private static final String TAG = FilmsPresenter.class.getSimpleName();
    private ViewContractor.ViewFilmsList viewFilmsList;
    private ObjectMapper objectMapper;

    public FilmsPresenter(ViewContractor.ViewFilmsList viewFilmsList) {
        this.viewFilmsList = viewFilmsList;
    }
    private void checkInternetConnection(){

    }

    @Override
    public void requestFilms() {
        objectMapper = new ObjectMapper();
        Log.d(TAG, "FILMS_URL: " + ConstantsUtils.BASE_URL + ConstantsUtils.FILMS);
        VolleyUtils.getMethodJsonRequest(ConstantsUtils.BASE_URL + ConstantsUtils.FILMS, new VolleyResponseListener() {
            @Override
            public void onResponse(Object response) {
                try {
                    Log.d(TAG, "FILMS_RES: " + response.toString());
                    JSONArray jsonArray = objectToJSONArray(response);//objectMapper.readValue(response.toString(), JSONArray.class);
                    List<String> moviesList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++){
                        try {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            moviesList.add(jsonObject.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (!moviesList.isEmpty()){
                        viewFilmsList.onMoviesList(moviesList);
                    } else {
                        viewFilmsList.onError("Operation Failed!");
                    }

                } catch (Exception e) {
                    viewFilmsList.onError("Operation Failed!");
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String message) {
                viewFilmsList.onError(message);
            }
        });
    }

    private JSONArray objectToJSONArray(Object object){
        Object json = null;
        JSONArray jsonArray = null;
        try {
            json = new JSONTokener(object.toString()).nextValue();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (json instanceof JSONArray) {
            jsonArray = (JSONArray) json;
        }
        return jsonArray;
    }
}
