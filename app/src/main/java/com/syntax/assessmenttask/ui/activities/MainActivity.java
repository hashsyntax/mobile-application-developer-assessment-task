package com.syntax.assessmenttask.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.syntax.assessmenttask.R;
import com.syntax.assessmenttask.ui.fragments.MainFragment;
import com.syntax.assessmenttask.utils.NetworkUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        setToolBar();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow();
        }
    }
    /**
     * @SET Toolbar for the back navigation
     * */
    public void setToolBar(boolean isEnable, String title) {
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(isEnable);
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*Check the Network connectivity*/
        if (!NetworkUtils.getInstance().isNetworkConnected(this)) {
            snackBarBottomBar(getString(R.string.no_network_connection));
        }
    }
    /**
     * This has been used in the fragment back press
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * @POP_ALERT shows at the bottom of the screen
     * */
    private void snackBarBottomBar(String responseString) {
        try {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.container), responseString, Snackbar.LENGTH_LONG);
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
