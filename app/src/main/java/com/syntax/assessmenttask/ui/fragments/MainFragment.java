package com.syntax.assessmenttask.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.syntax.assessmenttask.R;
import com.syntax.assessmenttask.adapters.FilmsAdapter;
import com.syntax.assessmenttask.interfaces.InternetConnectionContractor;
import com.syntax.assessmenttask.models.FilmsModelResponse;
import com.syntax.assessmenttask.presenters.FilmsPresenter;
import com.syntax.assessmenttask.ui.activities.MainActivity;
import com.syntax.assessmenttask.utils.NetworkUtils;
import com.syntax.assessmenttask.views.ViewContractor;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainFragment extends Fragment implements ViewContractor.ViewFilmsList,
        FilmsAdapter.FilmsClickListener,
        InternetConnectionContractor.ViewInternetConnection {
    @BindView(R.id.recyclerView_moviesList)
    RecyclerView moviesListRV;
    @BindView(R.id.progressBar1)
    ProgressBar progressbar;

    private FilmsAdapter filmsAdapter;
    private Context mContext;
    private List<FilmsModelResponse> filmsModelResponseList;
    private ViewContractor.FilmsListPresenter filmsListPresenter;

    public static MainFragment newInstance() {

        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filmsModelResponseList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_layout, container, false);
        ButterKnife.bind(this, rootView);
        mContext = getActivity();
        /*Register Internet Connection View*/
        NetworkUtils.getInstance().setViewInternetConnection(this);
        /*Request for Films/Movies List*/
        moviesListRequest();

        return rootView;
    }

    /**
     * NULL Checking
     * CHECKING Network
     */
    private void moviesListRequest() {
        try {
            if (filmsModelResponseList.isEmpty()) {
                if (NetworkUtils.getInstance().isNetworkConnected(mContext)) {
                    NetworkUtils.getInstance().checkForInternetConnection();
                } else {
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(mContext, "No Network Connected!", Toast.LENGTH_SHORT).show();
                }
            } else {
                progressbar.setVisibility(View.GONE);
                setFilmsAdapter(filmsModelResponseList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onInternetConnection(boolean internet) {
        if (internet) {
            filmsListPresenter = new FilmsPresenter(this);
            filmsListPresenter.requestFilms();
        } else {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(mContext, "No Internet Connected!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolBar(false, getString(R.string.app_name));
    }

    @Override
    public void onMoviesList(List<String> moviesList) {
        try {
            progressbar.setVisibility(View.GONE);
            ObjectMapper objectMapper = new ObjectMapper();
            for (String object : moviesList) {
                FilmsModelResponse filmsModelResponse = objectMapper.readValue(object, FilmsModelResponse.class);
                filmsModelResponseList.add(filmsModelResponse);
            }
            setFilmsAdapter(filmsModelResponseList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBadRequest() {

    }

    @Override
    public void onNotFound() {

    }

    @Override
    public void onError(String message) {
        progressbar.setVisibility(View.GONE);
        Toast.makeText(mContext, "No Internet Connected!", Toast.LENGTH_SHORT).show();
    }

    /**
     * @SET_ADAPTER list
     * @param filmsModelResponseList
     */
    private void setFilmsAdapter(List<FilmsModelResponse> filmsModelResponseList) {
        try {
            filmsAdapter = new FilmsAdapter(mContext, filmsModelResponseList, this);
            moviesListRV.setHasFixedSize(true);
            moviesListRV.setLayoutManager(new GridLayoutManager(mContext, 2, GridLayoutManager.VERTICAL, false));
            moviesListRV.setAdapter(filmsAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFilmItemClick(FilmsModelResponse filmsModelResponse) {
        replaceFragment(FilmFragment.newInstance(filmsModelResponse), FilmFragment.class.getSimpleName());
    }

    private void replaceFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .add(fragment, "FILM")
                // Add this transaction to the back stack
                .addToBackStack(null)
                .commit();
    }
    /**
     * @REPLAC fragment
     * */
    private void replaceFragment(Fragment fragment, String fragTag) {
        FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.replace(R.id.container, fragment, fragTag);
        mFragmentTransaction.commit();
    }

}
