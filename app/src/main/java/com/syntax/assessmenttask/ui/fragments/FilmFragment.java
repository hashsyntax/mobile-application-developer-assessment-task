package com.syntax.assessmenttask.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.syntax.assessmenttask.R;
import com.syntax.assessmenttask.models.FilmsModelResponse;
import com.syntax.assessmenttask.ui.activities.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilmFragment extends Fragment {
    private static final String FILM_MODEL = "FILM_MODEL";

    @BindView(R.id.textView_movieTitle)
    TextView movieTitleTV;
    @BindView(R.id.textView_movieDirector)
    TextView movieDirectorTV;
    @BindView(R.id.textView_movieProducer)
    TextView movieProducerTV;
    @BindView(R.id.textView_movieDescription)
    TextView movieDescriptionTV;
    @BindView(R.id.textView_movieRTScore)
    TextView movieRTScoreTV;


    private FilmsModelResponse filmsModelResponse;
    private Context mContext;
    /**
     * @param filmsModelResponse
     * */
    public static FilmFragment newInstance(FilmsModelResponse filmsModelResponse) {

        Bundle args = new Bundle();
        args.putParcelable(FILM_MODEL, filmsModelResponse);
        FilmFragment fragment = new FilmFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            filmsModelResponse = getArguments().getParcelable(FILM_MODEL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_film_layout, container, false);
        ButterKnife.bind(this, rootView);
        mContext = getActivity();
        /*Set Values*/
        setFilmDescription();

        return rootView;
    }
    /**
     * @SET Movies Values
     * */
    private void setFilmDescription(){
        try {
            movieTitleTV.setText(filmsModelResponse.getTitle());
            movieDirectorTV.setText(filmsModelResponse.getDirector());
            movieProducerTV.setText(filmsModelResponse.getProducer());
            movieDescriptionTV.setText(filmsModelResponse.getDescription());
            movieRTScoreTV.setText(filmsModelResponse.getRtScore());
        } catch (Exception e ){
            e.printStackTrace();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        /*SET toolbar Setting and title*/
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.setToolBar(true, filmsModelResponse.getTitle());
    }
}
