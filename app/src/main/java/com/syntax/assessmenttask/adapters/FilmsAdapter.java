package com.syntax.assessmenttask.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.syntax.assessmenttask.R;
import com.syntax.assessmenttask.models.FilmsModelResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilmsAdapter extends RecyclerView.Adapter<FilmsAdapter.FilmsViewHolder> {
    private Context mContext;
    private List<FilmsModelResponse> filmsModelResponseList;
    private FilmsClickListener filmsClickListener;
    public FilmsAdapter(Context context, List<FilmsModelResponse> filmsModelResponseList, FilmsClickListener filmsClickListener) {
        this.mContext = context;
        this.filmsModelResponseList = filmsModelResponseList;
        this.filmsClickListener = filmsClickListener;
    }

    @NonNull
    @Override
    public FilmsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.movies_list_row_layout, viewGroup, false);
        return new FilmsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FilmsViewHolder filmsViewHolder, int i) {
        try {
            final FilmsModelResponse filmsModelResponse = filmsModelResponseList.get(i);
            filmsViewHolder.releaseDateTxtV.setText(filmsModelResponse.getReleaseDate());
            filmsViewHolder.movieNameTxtV.setText(filmsModelResponse.getTitle());

            filmsViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    filmsClickListener.onFilmItemClick(filmsModelResponse);
                }
            });

           /* Glide.with(mContext)
                    .load(filmsModelResponse.getUrl())
                    .centerCrop()
                    .placeholder(R.drawable.ic_movie_black_24dp)
                    .into(filmsViewHolder.movieIconImgV);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return filmsModelResponseList.size();
    }

    public class FilmsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardview)
        View cardView;
        @BindView(R.id.textView_releaseDate)
        TextView releaseDateTxtV;
        @BindView(R.id.textView_movieName)
        TextView movieNameTxtV;
        @BindView(R.id.imageView_movieIcon)
        ImageView movieIconImgV;
        public FilmsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface FilmsClickListener{
        void onFilmItemClick(FilmsModelResponse filmsModelResponse);
    }
}
