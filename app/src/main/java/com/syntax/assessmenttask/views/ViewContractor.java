package com.syntax.assessmenttask.views;

import java.util.List;

public interface ViewContractor {
    /**
     * @FILMS views
     * Listeners to get the Films Array
     */
    interface ViewFilmsList {
        void onMoviesList(List<String> moviesList);
        void onBadRequest();
        void onNotFound();
        void onError(String message);
    }
    /**
     * @FILMS Interface
     * Request for Films
     * */
    interface FilmsListPresenter {
        void requestFilms();
    }
}
