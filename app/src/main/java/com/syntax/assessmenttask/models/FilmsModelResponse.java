package com.syntax.assessmenttask.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FilmsModelResponse implements Parcelable {
    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("director")
    private String director;
    @JsonProperty("producer")
    private String producer;
    @JsonProperty("release_date")
    private String releaseDate;
    @JsonProperty("rt_score")
    private String rtScore;
    @JsonProperty("people")
    private List<String> people = null;
    @JsonProperty("species")
    private List<String> species = null;
    @JsonProperty("locations")
    private List<String> locations = null;
    @JsonProperty("vehicles")
    private List<String> vehicles = null;
    @JsonProperty("url")
    private String url;

    public FilmsModelResponse() {
    }

    protected FilmsModelResponse(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        director = in.readString();
        producer = in.readString();
        releaseDate = in.readString();
        rtScore = in.readString();
        people = in.createStringArrayList();
        species = in.createStringArrayList();
        locations = in.createStringArrayList();
        vehicles = in.createStringArrayList();
        url = in.readString();
    }

    public static final Creator<FilmsModelResponse> CREATOR = new Creator<FilmsModelResponse>() {
        @Override
        public FilmsModelResponse createFromParcel(Parcel in) {
            return new FilmsModelResponse(in);
        }

        @Override
        public FilmsModelResponse[] newArray(int size) {
            return new FilmsModelResponse[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getRtScore() {
        return rtScore;
    }

    public void setRtScore(String rtScore) {
        this.rtScore = rtScore;
    }

    public List<String> getPeople() {
        return people;
    }

    public void setPeople(List<String> people) {
        this.people = people;
    }

    public List<String> getSpecies() {
        return species;
    }

    public void setSpecies(List<String> species) {
        this.species = species;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public List<String> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<String> vehicles) {
        this.vehicles = vehicles;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(director);
        parcel.writeString(producer);
        parcel.writeString(releaseDate);
        parcel.writeString(rtScore);
        parcel.writeStringList(people);
        parcel.writeStringList(species);
        parcel.writeStringList(locations);
        parcel.writeStringList(vehicles);
        parcel.writeString(url);
    }
}
