package com.syntax.assessmenttask.managers;

import android.support.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.syntax.assessmenttask.application.AssessmentTask;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

public class VolleyQueManager {
    private static VolleyQueManager _instance;
    private RequestQueue mRequestQueue;

    private VolleyQueManager() {
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyQueManager getInstance() {
        if (_instance == null) {
            _instance = new VolleyQueManager();
        }
        return _instance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getContext() is key, it keeps you from leaking the

//            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));

            VolleyLog.DEBUG = true;
            mRequestQueue = Volley.newRequestQueue(AssessmentTask.getAppContext());

        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(@NonNull final Request<T> request) {
        getRequestQueue().add(request);
    }

    public <T> void addToRequestQueue(@NonNull final Request<T> request, @NonNull final String tag) {
        request.setTag(tag);
        getRequestQueue().add(request);
    }

    public void cancelAllRequests(@NonNull final String tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void cancelRequest(String tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
