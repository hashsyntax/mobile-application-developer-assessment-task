package com.syntax.assessmenttask.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.syntax.assessmenttask.interfaces.InternetConnectionContractor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class NetworkUtils {
    private static final String TAG = "NetworkUtils";
    private static NetworkUtils instance;
    public static NetworkUtils getInstance(){
        if (instance == null){
            instance = new NetworkUtils();
        }
        return instance;
    }
    private InternetConnectionContractor.ViewInternetConnection viewInternetConnection;

    public void setViewInternetConnection(InternetConnectionContractor.ViewInternetConnection viewInternetConnection) {
        this.viewInternetConnection = viewInternetConnection;
    }
    /**
     * @NETWORK_STATE Checking
     * */
    public boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        try {
            networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // test for connection for WIFI
        if (networkInfo != null
                && networkInfo.isAvailable()
                && networkInfo.isConnected()) {
            return true;
        }

        networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // test for connection for Mobile
        if (networkInfo != null
                && networkInfo.isAvailable()
                && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
    /**
     * @INTERNET connection Testing
     * */
    public void checkForInternetConnection(){
        new InternetCheck();
    }
    /**
     * @INTERNET Connectivity Checking
     * */
    public class InternetCheck extends AsyncTask<Void, Void, Boolean> {

        InternetCheck() {
            execute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Socket sock = new Socket();
                sock.connect(new InetSocketAddress("8.8.8.8", 53), 1500);
                sock.close();
                return true;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean internet) {
            Log.d(TAG, "INTERNET_CONNECTION: " + internet);
            viewInternetConnection.onInternetConnection(internet);
        }
    }
}
