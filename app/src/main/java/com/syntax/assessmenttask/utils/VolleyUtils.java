package com.syntax.assessmenttask.utils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.syntax.assessmenttask.application.AssessmentTask;
import com.syntax.assessmenttask.interfaces.VolleyResponseListener;
import com.syntax.assessmenttask.managers.VolleyQueManager;

import org.json.JSONArray;

public class VolleyUtils {
    private static final String TAG = "VolleyUtils";
    private static int socketTimeout = 45000;//45 seconds - change to what you want
    public static RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    public static void getMethodJsonRequest(String url, final VolleyResponseListener listener) {
        // Initialize a new StringRequest
        JsonArrayRequest  stringRequest = new JsonArrayRequest(
                Request.Method.GET,
                url, null,
                new Response.Listener<JSONArray >() {
                    @Override
                    public void onResponse(JSONArray response) {
                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(VolleyErrorUtils.errorVolley(AssessmentTask.getAppContext(), error));
                    }
                });

        // Access the RequestQueue through singleton class.
        stringRequest.setRetryPolicy(policy);
        VolleyQueManager.getInstance().addToRequestQueue(stringRequest);
    }
}
