package com.syntax.assessmenttask.utils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.syntax.assessmenttask.R;

public class VolleyErrorUtils {
    public static String errorVolley(Context context, VolleyError volleyError) {
        volleyError.printStackTrace();
        String message = null;
        if (volleyError instanceof com.android.volley.TimeoutError || volleyError instanceof NoConnectionError) {
            message = context.getString(R.string.timeout_error_other);
        } else if (volleyError instanceof NetworkError) {
            message = context.getString(R.string.network_error);
        } else if (volleyError instanceof ServerError) {
            message = context.getString(R.string.server_error);
        } else if (volleyError instanceof AuthFailureError) {
            message = context.getString(R.string.auth_failure_error);
        } else if (volleyError instanceof ParseError) {
            message = context.getString(R.string.parse_error);
        }
        return message;
    }
}
