package com.syntax.assessmenttask.utils;

public class ConstantsUtils {
    public static final String BASE_URL = "https://ghibliapi.herokuapp.com";

    /*END POINTS*/
    public static final String FILMS = "/films";
}
