package com.syntax.assessmenttask.interfaces;

public interface VolleyResponseListener {
    /*
     * @Return the Volley Response
     * */
    void onResponse(Object response);

    /*
     * @Return The Error of Volley or from The server Response
     * */
    void onError(String message);
}
