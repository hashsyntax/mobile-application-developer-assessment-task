package com.syntax.assessmenttask.interfaces;

public interface InternetConnectionContractor {
    /**
     * @CHECK_INTERNET_CONNECTION
     * */
    interface ViewInternetConnection {
        void onInternetConnection(boolean internet);
    }
    /**
     * @CHECK_INTERNET_CONNECTION Listener
     * */
    interface InternetConnectionPresenter{
        void checkInternetConnection();
    }
}
